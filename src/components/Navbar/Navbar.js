import React, { useState, useEffect } from 'react';
import { Button } from '../parts/Button/Button';
import { Link } from 'react-router-dom';
import './Navbar.css';
import { MdFingerprint } from 'react-icons/md';
import { FaBars, FaTimes } from 'react-icons/fa';
import { IconContext } from 'react-icons/lib';

function Navbar() {
  const [click, setClick] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  useEffect(() => {
    showButton();
  }, []);

  window.addEventListener('resize', showButton);

  return (
    <>
      <IconContext.Provider value={{ color: '#fff' }}>
        <nav className='navbars'>
          <div className='navbar-containers containers'>
            <Link to='/' className='navbar-logos' style={{textDecoration: 'none'}}  onClick={closeMobileMenu}>
              <MdFingerprint className='navbar-icons' />
              WIN
            </Link>
            <div className='menu-icons' onClick={handleClick}>
              {click ? <FaTimes /> : <FaBars />}
            </div>
            <ul className={click ? 'nav-menus active' : 'nav-menus'}>
              {/* <li className='nav-items'>
                <Link to='/' className='nav-links' style={{textDecoration: 'none'}}  onClick={closeMobileMenu}>
                  Home
                </Link>
              </li> */}
              <li className='nav-items'>
                <Link
                  to='/services'
                  className='nav-links' style={{textDecoration: 'none'}}
                  onClick={closeMobileMenu}
                >
                  Kontak
                </Link>
              </li>
              <li className='nav-items'>
                <Link
                  to='/products'
                  className='nav-links' style={{textDecoration: 'none'}}
                  onClick={closeMobileMenu}
                >
                  Masuk
                </Link>
              </li>
              {/* <li className='nav-btns'>
                {button ? (
                  <Link to='/sign-up' className='btn-links' style={{textDecoration: 'none'}} >
                    <Button buttonStyle='btn--outline'>Masuk</Button>
                  </Link>
                ) : (
                  <Link to='/sign-up' className='btn-links'>
                    <Button
                      buttonStyle='btn--outline'
                      buttonSize='btn--mobile'
                      onClick={closeMobileMenu}
                    >
                      SIGN UP
                    </Button>
                  </Link>
                )}
              </li> */}
            </ul>
          </div>
        </nav>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;
