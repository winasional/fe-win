import React from 'react';
import { Link } from 'react-router-dom';
import { MdFingerprint } from 'react-icons/md';
import { FaFacebook, FaInstagram, FaMailBulk} from 'react-icons/fa';
import { Accordion, Card } from 'react-bootstrap';
import { useAccordionToggle } from 'react-bootstrap/AccordionToggle';

import './Footer.css';
import { Button } from '../../parts/Button/Button';

function Footer() {
  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <p className='footer-subscription-heading'>
        Mengapa harus membicarakan tentang reseller?
        </p>
        <p className='footer-subscription-text'>
        (Deskripsi)
        </p>
      </section>
      <div>
        <h2 className='title'>Hubungi Kami</h2>
      </div>
      <div className='footer-links'>
        <div className='footer-link-wrapper'>
          <div className='footer-link-items'>
            <p>Alamat : </p>
            <Link to='/'>Jl. Raya Kebun Manggis 5 No.666 RT/07 RW/07</Link>
          </div>
          <div className='footer-link-items'>
            <p>Tentang Kami</p>
          </div>
          <div className='footer-link-items '>
            <Link to='/'>
            <FaInstagram size={24}/>
              &nbsp;&nbsp; Instagram
            </Link>
            <Link to='/' className='social-icon-link'>
              <FaFacebook size={24}/>
              &nbsp;&nbsp; Facebook
            </Link>
            <Link to='/'>
              <FaMailBulk size={24}/>
              &nbsp;&nbsp; Email
            </Link>
          </div>
        </div>
      </div>
      <section className='footer-subscription'>
        <h3>FAQ</h3>
        <div className="accordion-view">
          <Accordion defaultActiveKey="0">
            <Card>
              <Accordion.Toggle as={Card.Header} eventKey="0">
                Click me!
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="0">
                <Card.Body>Hello! I'm the body</Card.Body>
              </Accordion.Collapse>
            </Card>
            <Card>
              <Accordion.Toggle as={Card.Header} eventKey="1">
                Click me!
              </Accordion.Toggle>
              <Accordion.Collapse eventKey="1">
                <Card.Body>Hello! I'm another body</Card.Body>
              </Accordion.Collapse>
            </Card>
          </Accordion>
        </div>
      </section>
    </div>
  );
}

export default Footer;
