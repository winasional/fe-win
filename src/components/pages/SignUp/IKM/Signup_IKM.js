import React, {useState} from 'react';
import { Col, Container, Row, Form, Button } from 'react-bootstrap';
import './Signup_IKM.css';
import { Link } from 'react-router-dom';

import PageviewRoundedIcon from '@material-ui/icons/PageviewRounded';
import MonetizationOnRoundedIcon from '@material-ui/icons/MonetizationOnRounded';
import AccountBalanceWalletRoundedIcon from '@material-ui/icons/AccountBalanceWalletRounded';


const SignUpIKM=(props) => {
    return (

        <Container fluid className='sgn-ikm'>
            <Button>
                <a id="one">Section 1</a>
            </Button>
            <Button onClick={props.onClick}>
                <a id="two">Section 2</a>
            </Button>

     
                <div class="sym">
                    <ul>
                        <li><a href="#two">Section 2</a></li>
                    </ul>
                </div>

            <section id="two">
            <h1>Section 2</h1>
            </section>

        </Container>

        );
    }

export default SignUpIKM;
