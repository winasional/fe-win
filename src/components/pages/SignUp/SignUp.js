// import React from 'react';
// import HeroSection from '../../HeroSection/HeroSection';
// import { homeObjOne, homeObjTwo, homeObjThree, homeObjFour } from './Data';
// import Pricing from '../../Pricing/Pricing';

// function SignUp() {
//   return (
//     <>
//       <HeroSection {...homeObjOne} />
//       {/* <HeroSection {...homeObjThree} /> */}
//     </>
//   );
// }

// export default SignUp;

import React, {useState} from 'react';
import { Col, Container, Row, Form, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Signup.css';
import Drawer from './IKM/Signup_IKM';


import PageviewRoundedIcon from '@material-ui/icons/PageviewRounded';
import MonetizationOnRoundedIcon from '@material-ui/icons/MonetizationOnRounded';
import AccountBalanceWalletRoundedIcon from '@material-ui/icons/AccountBalanceWalletRounded';




const SignUp = () => {
    const [drawerOpen, setDrawerOpen] = useState(false)
    const handleDrawerOpen = () => {
      if (drawerOpen) {
        setDrawerOpen(true)
      } else {
        setDrawerOpen(false)
      }
    }
    return (

        <Container fluid className='sg-signup'>
            <Row>
              <Button onClick={Drawer.handleDrawerOpen}>
                Daftar IKM
              </Button>
            </Row>
            <Row>
                <Col>
                    <Container className='signup-box'>
                        {/* <Row>
                            <Col sm='6'> */}
                            <Form>
                                <h1 className='titleH1signup'>DAFTAR SEBAGAI MITRA</h1>
                                  <Form.Group controlId="formBasicDaftar">
                                  <Form.Group>
                                    <Row>
                                      <Col>
                                        <Form.Control placeholder="First name" />
                                      </Col>
                                      <Col>
                                        <Form.Control placeholder="Last name" />
                                      </Col>
                                    </Row>
                                    </Form.Group>
                                    <Form.Group>
                                      <Form.Control placeholder="Username" />
                                    </Form.Group>
                                    <Form.Group>
                                      <Form.Control placeholder="No.Handphone"/>
                                    </Form.Group>
                                    <Form.Group>
                                    {/* <Form.Label>Email address</Form.Label> */}
                                      <Form.Control type="email" placeholder="Enter email" />
                                    </Form.Group>
                                    <Form.Group>
                                      <Form.Control type="password" placeholder="Password" />
                                    </Form.Group>
                                    <Form.Group>
                                      <Form.Control type="password" placeholder="Re-Password" />
                                    </Form.Group>
                                    {/* <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                    </Form.Text> */}
                                </Form.Group>

                                {/* <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" />
                                </Form.Group>
                                <Form.Group controlId="formBasicCheckbox">
                                    <Form.Check type="checkbox" label="Check me out" />
                                </Form.Group> */}
                                <Row>
                                    <Col>
                                        <Link to='/'>
                                            <Button variant="primary" type="submit" className="btn-signmsk-boots">
                                                Masuk
                                            </Button>
                                        </Link>
                                    </Col>

                                </Row>
                            </Form>
                            {/* </Col>
                        </Row> */}

                    </Container>
                </Col>
                <Col>
                    <Container className='adsv-box-sgnup'>
                        <PageviewRoundedIcon style={{ fontSize: 30 }}/> On Time
                        <br/><MonetizationOnRoundedIcon style={{ fontSize: 30 }}/> Good Trades
                        <br/><AccountBalanceWalletRoundedIcon style={{ fontSize: 30 }}/> Pay Online
                    </Container>
                </Col>
            </Row>
        </Container>

        );
    }

export default SignUp;
