import React from 'react';
import { Link } from 'react-router-dom';
import { MdFingerprint } from 'react-icons/md';
import { FaFacebook, FaInstagram, FaMailBulk} from 'react-icons/fa';

import './Footer.css';
import {footerObjOne, footerObjTwo, footerObjThree, footerObjFour, footerObjFive} from './Data'
import {Accordion} from '../../Accordion/Accordion'
import { Button } from '../../parts/Button/Button';

function Footer() {
  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <p className='footer-subscription-heading'>
        Mengapa harus membicarakan tentang reseller?
        </p>
        <p className='footer-subscription-text'>
        (Deskripsi)
        </p>
      </section>
      <div>
        <h2 className='title'>Hubungi Kami</h2>
      </div>
      <div className='footer-links'>
        <div className='footer-link-wrapper'>
          <div className='footer-link-items'>
            <p>Alamat : </p>
            <Link to='/'>Jl. Raya Kebun Manggis 5 No.666 RT/07 RW/07</Link>
          </div>
          <div className='footer-link-items'>
            <p>Tentang Kami</p>
          </div>
          <div className='footer-link-items '>
            <Link to='/'>
            <FaInstagram size={24}/>
              &nbsp;&nbsp; Instagram
            </Link>
            <Link to='/' className='social-icon-link'>
              <FaFacebook size={24}/>
              &nbsp;&nbsp; Facebook
            </Link>
            <Link to='/'>
              <FaMailBulk size={24}/>
              &nbsp;&nbsp; Email
            </Link>
          </div>
        </div>
      </div>
      <section className='footer-subscription'>
        <h3 className="title-faq">FAQ</h3>
        <div className="wrapper">
          <Accordion {...footerObjOne} />
          <Accordion {...footerObjTwo} />
          <Accordion {...footerObjThree} />
          <Accordion {...footerObjFour} />
          <Accordion {...footerObjFive} />
        </div>
      </section>
    </div>
  );
}

export default Footer;
