import React from 'react';
import { Col, Container, Row, Form, Button } from 'react-bootstrap';
import './login.css';
import { Link } from 'react-router-dom';

import PageviewRoundedIcon from '@material-ui/icons/PageviewRounded';
import MonetizationOnRoundedIcon from '@material-ui/icons/MonetizationOnRounded';
import AccountBalanceWalletRoundedIcon from '@material-ui/icons/AccountBalanceWalletRounded';


function loginWin() {
    return (

        <Container fluid className='bg-login'>
            <Row>
                <Col>
                    <Container className='adsv-box'>
                        <PageviewRoundedIcon style={{ fontSize: 30 }}/> Follow Your Interests
                        <br/><MonetizationOnRoundedIcon style={{ fontSize: 30 }}/> Buy Everything You Need
                        <br/><AccountBalanceWalletRoundedIcon style={{ fontSize: 30 }}/> Still save money
                    </Container>
                </Col>
                <Col>
                    <Container className='login-box'>
                        {/* <Row>
                            <Col sm='6'> */}

                            <Form>
                                <h1 className='titleH1'>MASUK</h1>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control type="email" placeholder="Enter email" />
                                    <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                    </Form.Text>
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" />
                                </Form.Group>
                                <Form.Group controlId="formBasicCheckbox">
                                    <Form.Check type="checkbox" label="Check me out" />
                                </Form.Group>
                                <Row>
                                    <Col>
                                        <Link to='/'>
                                            <Button variant="primary" type="submit" className="btn-logsub-boots">
                                                Masuk
                                            </Button>
                                        </Link>
                                    </Col>
                                    <Col>
                                        <Link to='/sign-up'>
                                            <Button variant="primary" type="button" className="btn-daftar-boots">
                                                Daftar
                                            </Button>
                                        </Link>
                                    </Col>
                                </Row>
                            </Form>
                            {/* </Col>
                        </Row> */}

                    </Container>
                </Col>
            </Row>
        </Container>

        );
    }

export default loginWin;