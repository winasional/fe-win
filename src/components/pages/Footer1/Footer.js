import React from 'react';
import { Link } from 'react-router-dom';
import { MdFingerprint } from 'react-icons/md';
import { FaFacebook, FaInstagram, FaMailBulk} from 'react-icons/fa';
import {Accordion} from './Accordion'
// import { Accordion, Card } from 'react-bootstrap';
// import { useAccordionToggle } from 'react-bootstrap/AccordionToggle';

import './Footer.css';
import { Button } from '../../parts/Button/Button';

function Footer() {
  return (
    <div className='footer-container'>
      <section className='footer-subscription'>
        <p className='footer-subscription-heading'>
        Mengapa harus membicarakan tentang reseller?
        </p>
        <p className='footer-subscription-text'>
        (Deskripsi)
        </p>
      </section>
      <div>
        <h2 className='title'>Hubungi Kami</h2>
      </div>
      <div className='footer-links'>
        <div className='footer-link-wrapper'>
          <div className='footer-link-items'>
            <p>Alamat : </p>
            <Link to='/'>Jl. Raya Kebun Manggis 5 No.666 RT/07 RW/07</Link>
          </div>
          <div className='footer-link-items'>
            <p>Tentang Kami</p>
          </div>
          <div className='footer-link-items '>
            <Link to='/'>
            <FaInstagram size={24}/>
              &nbsp;&nbsp; Instagram
            </Link>
            <Link to='/' className='social-icon-link'>
              <FaFacebook size={24}/>
              &nbsp;&nbsp; Facebook
            </Link>
            <Link to='/'>
              <FaMailBulk size={24}/>
              &nbsp;&nbsp; Email
            </Link>
          </div>
        </div>
      </div>
      <section className='footer-subscription'>
        <h3 style={{textAlign: 'center', marginTop: '10px'}}>FAQ</h3>
        <div className="wrapper">
        <Accordion title="Bagaimana saya bisa Login?">
          Lorem ipsum dolor sit amet consectetur adipisicing elit.
          Deleniti nesciunt assumenda officiis corporis aliquam alias doloremque culpa sequi expedita minus.
        </Accordion>
        <Accordion title="Bagaimana cara kerja WIN ini?">
          It's really hot inside Jupiter! No one knows exactly how hot, but
          scientists think it could be about 43,000°F (24,000°C) near Jupiter's
          center, or core.
        </Accordion>
        <Accordion title="Kendala apa yang bisa terjadi?">
          A black hole is an area of such immense gravity that nothing -- not even
          light -- can escape from it.
        </Accordion>
        <Accordion title="Apa itu status pada profil?">
          A black hole is an area of such immense gravity that nothing -- not even
          light -- can escape from it.
        </Accordion>
        <Accordion title="Bisakah mengganti status?">
          A black hole is an area of such immense gravity that nothing -- not even
          light -- can escape from it.
        </Accordion>
      </div>
      </section>
    </div>
  );
}

export default Footer;
